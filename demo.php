<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>

		<title>Tumblr Note Stats - Demo</title>

		<link rel="stylesheet" type="text/css" href="css/styles.css">

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

		<script type="text/javascript" src = "js/script.js"></script>

		<script type="text/javascript">

		  var _gaq = _gaq || [];

		  _gaq.push(['_setAccount', 'UA-31887442-1']);

		  _gaq.push(['_setDomainName', 'michaelku.com']);

		  _gaq.push(['_trackPageview']);



		  (function() {

		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

		  })();



		</script>

	</head>

	<body>



<?php

	$notes = file_get_contents('notes.txt', true);
	$file_name = "notes.txt";
	$file_size = "26903";
	$file_type = "text/plain";


/**************/

/* Navigation */

/**************/

		//Explodes notes by ending list tag for a count

		//Does not count the last item (ghost item)

		$by_note = explode("</li>", $notes, -1);



		//Not sure if I need this but whatever

		$note_count = count($by_note);



		print "

			<div id = 'container'>

				<div id = 'header'>

					<div id = 'file_info'>

						<div class = 'info_row'>

							<div class = 'info_title'> File name: </div>

							<div class = 'info_body'> $file_name </div>

						</div>

						<div class = 'info_row'>

							<div class = 'info_title'> Size: </div>

							<div class = 'info_body'> $file_size kb</div>

						</div>

						<div class = 'info_row'>

							<div class = 'info_title'> Type: </div>

							<div class = 'info_body'> $file_type </div>

						</div>

						<div class = 'info_row'>

							<div class = 'info_title'> Total Notes: </div>

							<div class = 'info_body'> $note_count </div>

						</div>

					</div>

					<div id = 'note_nav'>

						<div class = 'display_option'> <a id = 'go_home' href = 'index.php'>Upload New</a> </div>

						<div class = 'display_option'> <a id = 'display_all' href = '#'>All Notes</a> </div>

						<div class = 'display_option'> <a id = 'display_most' href = '#'>Most Activity</a> </div>

						<div class = 'display_option'> <a id = 'display_top' href = '#'>Top Sources</a> </div>

					</div>

				</div>

				<div id = 'display'>

					<div id = 'all' class = 'minimal_frame'>

		";



		//Start table

		print "

			<table class = 'table'>

				<tr class = 'row'>

					<td class = 'header column'>Notes</td>

					<td class = 'header column'>Poster</td>

					<td class = 'header column'>Action</td>

					<td class = 'header column'>Source</td>

				</tr>

		";



		//Note count needed for looping later on

		$i = $note_count;



		//Array of note elements

		$poster_array = array();

		$source_array = array();

		$more_array = array();



/**********************/

/* Begin note parsing */

/**********************/



		//Loop through notes

		foreach($by_note as $singular_note){



			$note = explode('class="action"', $singular_note);



			//Scan note for these keywords

			//Not sure if it will break if these words are used in text format

			$remainder = $note[1];

			$reblog = "/reblogged/";

			$like = "/likes/";

			$replied = "/said/";

			$posted = "/posted/";



			$if_reblog = preg_match($reblog, $remainder);

			$if_like = preg_match($like, $remainder);

			$if_replied = preg_match($replied, $remainder);

			$if_posted = preg_match($posted, $remainder);



			//If REBLOG

			if($if_reblog == true){

				//Get Permalink

				$link = explode('data-post-url="', $note[1]);

				$link = explode('">', $link[1]);

				$link = $link[0];



				//Get Poster Name

				$note = explode('href="http://', $note[1]);

				$poster = explode('.tumblr.com/', $note[1]);

				$poster = $poster[0];



				//Custom Poster?

				$custom_poster = explode('/', $poster);

				$poster = $custom_poster[0];



				//Poster Array Push

				array_push($poster_array, $poster);



				//Get Reblog Source

				$source = explode('.tumblr.com/', $note[2]);

				$source = $source[0];



				//Custom Source?

				$custom_source = explode('/', $source);

				$source = $custom_source[0];



				//Source Array Push

				array_push($source_array, $source);

				

				//Check to see if there is added text.

				if(isset($note[3])){

					$more = explode('">', $note[3]);

					$more = $more[1];

				}

				else{

					$more = "none";

				}



				//More Array Push

				array_push($more_array, $more);



				print "

					<tr class = 'row'>

						<td class = 'number column'>$i</td>

						<td class = 'poster column'><a href = 'http://$poster.tumblr.com/'>$poster</a></td>

						<td class = 'action column'><a href = '$link'>reblogged</a></td>

						<td class = 'source column'><a href = 'http://$source.tumblr.com/'>$source</a></td>

					</tr>

				";



				if($more !== "none"){

					print "

						<tr class = 'more row'>

							<td></td>

							<td colspan = '3' class = 'text'>&raquo; $more</td>

						</tr>

					";

				}

			}



			else if($if_replied == true){

				//Get Poster Name

				$note = explode('href="http://', $note[1]);

				$poster = explode('.tumblr.com/', $note[1]);

				$poster = $poster[0];



				//Custom Poster?

				$custom_poster = explode('/', $poster);

				$poster = $custom_poster[0];



				//Poster Array Push

				array_push($poster_array, $poster);

				

				$reply_text = explode('<span class="answer_content">', $note[1]);

				$reply_text = explode('</span>', $reply_text[1]);

				$more = $reply_text[0];



				//More Array Push

				array_push($more_array, $more);



				print "

					<tr class = 'row'>

						<td class = 'number column'>$i</td>

						<td class = 'poster column'><a href = 'http://$poster.tumblr.com/'>$poster</a></td>

						<td class = 'action column'><a href = '$link'>replied</a></td>

				";



				if($more !== "none"){

					print "

						<tr class = 'more row'>

							<td></td>

							<td colspan = '3' class = 'text'>&raquo; $more</td>

						</tr>

					";

				}



				print "</tr>";



			}



			//If LIKE or POSTED (no additional information)

			else{

				$note = explode('href="http://', $note[1]);

				$poster = explode('.tumblr.com/', $note[1]);

				$poster = $poster[0];



				//Custom Poster?

				$custom_poster = explode('/', $poster);

				$poster = $custom_poster[0];



				//Poster Array Push

				array_push($poster_array, $poster);



				if($if_like == true){

					$message = "liked this post";

				}



				else if($if_posted == true){

					$message = "posted this";

				}



				print "

					<tr class = 'row'>

						<td class = 'number column'>$i</td>

						<td class = 'poster column'><a href = 'http://$poster.tumblr.com/'>$poster</a></td>

						<td class = 'action column'>$message</td>

					</tr>

				";

			}



			//$i++;

			$i--;;;

		}

		

		print "</table>";



		print "</div>";



/********************/

/* Most Notes Table */

/********************/



		$posters = array_count_values($poster_array);

		arsort($posters);



		print "

			<div id = 'most' class = 'minimal_frame'>

		";



		print "

			<table class = 'table'>

				<tr class = 'row'>

					<td class = 'header column'>Poster</td>

					<td class = 'header column'>Count</td>

				</tr>

		";



		foreach($posters as $poster_info => $values){

			print "

				<tr class = 'row'>

					<td class = 'poster column'><a href = 'http://$poster_info.tumblr.com/'>$poster_info</a></td>

					<td class = 'number column'>$values</td>



				</tr>

			";

		}



		print "</table> <br />";



		print "</div>";



/*********************/

/* Top Sources Table */

/*********************/



		$sources = array_count_values($source_array);

		arsort($sources);

		

		print "

			<div id  = 'top' class = 'minimal_frame'>

		";



		print "

			<table class = 'table'>

				<tr class = 'row'>

					<td class = 'header column'>Poster</td>

					<td class = 'header column'>Referals</td>

				</tr>

		";



		foreach($sources as $source_info => $values){

			print "

				<tr class = 'row'>

					<td class = 'source column'><a href = 'http://$source_info.tumblr.com/'>$source_info</a></td>

					<td class = 'number column'>$values</td>



				</tr>

			";

		}



		print "</table>";



		print "</div>";

?>

			</div>

		</div>

	</body>

</html>