<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Tumblr Note Stats v2 - Michael Ku</title>
		<link rel="stylesheet" type="text/css" href="css/reset.css">
		<link rel="stylesheet" type="text/css" href="css/styles.min.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-31887442-1']);
		  _gaq.push(['_setDomainName', 'michaelku.com']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	</head>
	<body>
		<div id = "container">
			<div id = "header">
				<h1>tumblr note stats v2</h1>
			</div>
			<div id = "content">
				<div id = "body">
					<form id = "uploadForm" action = "stats.php" method = "post" enctype="multipart/form-data">
						<input type = "file" name = "file" id = "file">
						<input type = "submit" name = "submit" id = "notes_submit" value = "Get Stats!" />
					</form>

					<div id = "instructions">
						<strong>How to Use:</strong>
						<ol>
							<li>Manually load all notes for a post by going to the posts permalink and spamming the fuck out of "Show more Notes"</li>
							<li>In Google Chrome, right-click and use the "Inspect Element" tool, and using CTRL+F, search for <code>&lt;ol class="notes"&gt;</code></li>
							<li>In Firefox, do the same but when the toolbar comes up, click the second button from the left, find <code>&lt;ol class="notes"&gt;</code>, and choose the "Copy Outer HTML" option</li>
							<li>Right-click the node and click the "Copy as HTML" option</li>
							<li>Paste the code in Notepad a similar program and save as a .TXT file</li>
							<li>Upload file via form above</li>
						</ol>
						<a href = "demo.php">Demo</a>
					</div>
				</div>
			</div>
			<div id = "footer">
				Created by <a href = "http://michaelku.com">Michael Ku</a></p>
			</div>
		</div>
	</body>
</html>