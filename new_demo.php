<?php

/**********************************/
/* Step 1: Get all data from file */
/**********************************/

	//START TIMER #1
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;

	$notes = file_get_contents('notes.txt', true);
	$notes = trim($notes);

	//Arrays
	$poster_name_array = array();
	$poster_link_array = array();
	$source_name_array = array();
	$source_link_array = array();
	$action_array = array();
	$permalink_array = array();
	$extra_array = array();
	
	//Split by note (<li>)
	$by_note = explode("</li>", $notes, -1);

	foreach($by_note as $singular_note){
		$note = explode('class="action"', $singular_note);

		$inner_data = $note[1];

		$data_split = explode("</span>", $inner_data);
		$post_data = $data_split[0];
		$post_data = explode(">", $post_data);

		$extra_data = $data_split[1];
		$extra_data = explode('<div class="clear"></div>', $extra_data);

		$temp_permalink = trim($post_data[0]);
		$temp_poster_link = trim($post_data[1]);
		$temp_source_link = trim($post_data[3]);
		$temp_action = trim($post_data[3]);
		$temp_extra = trim($extra_data[1]);
		$temp_poster = trim($post_data[2]);
	
		$poster = explode('<', $temp_poster);
		$poster = trim($poster[0]);

		$has_permalink = strlen($temp_permalink);

		//Action
		$action = explode(" ", $temp_action);
		$action = trim($action[0]);
		array_push($action_array, $action);
		
		$action_split = explode(" ", $action);
		$action_split = $action_split[0];

		if($action_split == "reblogged"){
			$permalink = explode('"', $temp_permalink);
			$permalink = trim($permalink[1]);

			//Source + Source Link
			$temp_source = $post_data[4];
			$source = explode('<', $temp_source);
			$source = trim($source[0]);
			$source_link = explode('href="', $temp_source_link);
			$source_link = explode('"', $source_link[1]);
			$source_link = trim($source_link[0]);
		}
		else{
			$permalink = "none";
			$source = "none";
			$source_link = "none";
		}
		array_push($permalink_array, $permalink);
		array_push($source_name_array, $source);
		array_push($source_link_array, $source_link);

		//Poster + Poster Link
		$poster_link = explode('href="', $temp_poster_link);
		$poster_link = explode('"', $poster_link[1]);
		$poster_link = trim($poster_link[0]);

		array_push($poster_name_array, $poster);
		array_push($poster_link_array, $poster_link);

		//Extra
		$extra_count = strlen($temp_extra);
		if($extra_count > 0){
			$extra = explode('"View post">', $temp_extra);
			$extra = explode('</a>', $extra[1]);
			$extra = trim($extra[0]);
		}
		else{
			$extra = "none";
		}
		array_push($extra_array, $extra);
	}

	//STOP TIMER #1
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo 'Step #1 took '.$total_time.' seconds. <br />';

/**************************/
/* Step 2: Reverse Arrays */
/**************************/

	//START TIMER #2
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;

	$poster_name_array = array_reverse($poster_name_array);
	$poster_link_array = array_reverse($poster_link_array);
	$source_name_array = array_reverse($source_name_array);
	$source_link_array = array_reverse($source_link_array);
	$action_array = array_reverse($action_array);
	$permalink_array = array_reverse($permalink_array);
	$extra_array = array_reverse($extra_array);

	$notes_count = count($poster_name_array);
	//echo $notes_count;

	for($i = 0; $i < $notes_count; $i++){
		
		//Print poster name and link
		print "
			<a href = '$poster_link_array[$i]'>
				$poster_name_array[$i]
			</a>
		";

		//Determine and print action and source name & link if applicable
		if($action_array[$i] == "posted"){
			print "posted this";
		}
		elseif($action_array[$i] == "likes"){
			print "liked this";
		}
		elseif($action_array[$i] == "reblogged"){
			print "
				<a href = '$permalink_array[$i]'>
					reblogged
				</a>
				 this from 
				<a href = '$source_link_array[$i]'>
					$source_name_array[$i]
				</a>
			";
		}

		//Determine and print extras
		if($extra_array[$i] != "none"){
			print "
				and added $extra_array[$i]
			";
		}

		print "<br />";
	}

	//STOP TIMER #2
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo 'Step #2 took '.$total_time.' seconds. <br />';

/************************/
/* Step 3: Backtracking */
/************************/

	//START TIMER #3
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;

	$tree_array = array();

	//Removes duplicate entries from poster array for function below
	$unique_posters_array = array_unique($poster_name_array);

	//Basically, for each unique poster, check for the poster
	//in the source array and get the poster.
	foreach ($unique_posters_array as $poster) {
		$temp_posters_array = array();
		$branch_array = array();

		$notes_count = count($source_name_array);
		for($i = 0; $i < $notes_count; $i++){
			if($source_name_array[$i] == $poster){
				array_push($temp_posters_array, $poster_name_array[$i]);
			}
		}
		
		$hits_count = count($temp_posters_array);
		if($hits_count > 0){
			foreach ($temp_posters_array as $hit) {
				array_push($branch_array, $hit);
				$tree_array[$poster] = $branch_array;
			}
		}
	}

	//Loop through all source posters
	$sources = array_keys($tree_array);
	$original_poster = $sources[0];

	$op_sourced = count($tree_array[$original_poster]);

	print "$original_poster - <br />";

	for ($i = 0; $i < $op_sourced; $i++) { 
		$temp_branch = $tree_array[$original_poster][$i];
		print "=> $temp_branch";

		foreach ($tree_array as $branch => $branch_array) {
			$branch_count = count($branch_array);

			if($temp_branch == $branch){
				for ($j = 0; $j < $branch_count; $j++){
					print "
						 <br />====> $branch_array[$j]
					";
				}
			}
		}

		print "<br />";
	}
/*
	foreach ($tree_array as $branch => $branch_array) {
		$branch_count = count($branch_array);

		print "$branch - ";
		for ($i = 0; $i < $branch_count; $i++){
			print "
				$branch_array[$i] <br />
			";
		}
	}
*/

	//STOP TIMER #3
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	echo 'Step #3 took '.$total_time.' seconds. <br />';
?>