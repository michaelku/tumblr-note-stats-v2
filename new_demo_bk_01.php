<?php

	$notes = file_get_contents('notes.txt', true);

	//Arrays
	$poster_name_array = array();
	$poster_link_array = array();
	$source_name_array = array();
	$source_link_array = array();
	$action_array = array();
	$permalink_array = array();
	$extra_array = array();

	
	//Split by note (<li>)
	$by_note = explode("</li>", $notes, -1);

	foreach($by_note as $singular_note){
		$note = explode('class="action"', $singular_note);

		$inner_data = $note[1];

		//echo $inner_data."END";

		$data_split = explode(">", $inner_data);
		$temp_permalink = $data_split[0];
		$temp_action = $data_split[3];
		$poster = $data_split[2];
		$source = $data_split[4];
	
		//Has Permalink
		$has_permalink = strlen($temp_permalink);
		if($has_permalink > 0){
			$permalink = explode('"', $temp_permalink);
			$permalink = $permalink[1];
		}
		else{
			$permalink = "none";
		}
		array_push($permalink_array, $permalink);

		//Poster
		array_push($poster_name_array, $poster);

		//Action
		$action = explode(" ", $temp_action);
		$action = $action[1];
		array_push($action_array, $action);

		//Source
		array_push($source_name_array, $source);
	}