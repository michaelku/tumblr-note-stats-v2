<?php

	$notes = file_get_contents('notes.txt', true);

	//Arrays
	$poster_name_array = array();
	$poster_link_array = array();
	$source_name_array = array();
	$source_link_array = array();
	$action_array = array();
	$permalink_array = array();
	$extra_array = array();

	
	//Split by note (<li>)
	$by_note = explode("</li>", $notes, -1);

	foreach($by_note as $singular_note){
		$note = explode('class="action"', $singular_note);

		$inner_data = $note[1];

		//echo $inner_data."END";

		$data_split = explode("</span>", $inner_data);
		$poster_data = $data_split[0];
		$poster_data = explode(">", $poster_data);

		$temp_permalink = $poster_data[0];
		$temp_poster_link = $poster_data[1];
		$temp_source_link = $poster_data[3];
		$temp_action = $poster_data[3];
		$poster = $poster_data[2];
	
		$has_permalink = strlen($temp_permalink);


		//print "NUM = $has_permalink <code>$poster_data[0]</code>";
		//print "<code>$poster_data[1]</code>";
		//print "<code>$poster_data[2]</code>";
		//print "<code>$poster_data[3]</code>";
		//print "<code>$poster_data[4]</code><br /><br />";

		//Action
		$action = explode(" ", $temp_action);
		$action = $action[1];
		array_push($action_array, $action);

		//Has Permalink
		$has_permalink = strlen($temp_permalink);
		
		$action_split = explode(" ", $action);
		$action_split = $action_split[0];

		if($action_split == "reblogged"){
			if($has_permalink > 0){
				$permalink = explode('"', $temp_permalink);
				$permalink = $permalink[1];
				$source = $poster_data[4];

				//Poster + Poster Link
				$source_link = explode('href="', $temp_source_link);
				$source_link = explode('"', $source_link[1]);
				$source_link = $source_link[0];
			}
			else{
				$permalink = "none";
				$source = "none";
				$source_link = "none";
			}
		}
		array_push($permalink_array, $permalink);
		array_push($source_name_array, $source);
		array_push($source_link_array, $source_link);

		//Poster + Poster Link
		$poster_link = explode('href="', $temp_poster_link);
		$poster_link = explode('"', $poster_link[1]);
		$poster_link = $poster_link[0];

		array_push($poster_name_array, $poster);
		array_push($poster_link_array, $poster_link);
	}